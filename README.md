# Minetest Mod - TimeDay

**TimeDay** is a [Minetest](https://www.minetest.net/) mod that displays current time and days since start on bottom right of screen.


For installation, see https://content.minetest.net/help/installing/