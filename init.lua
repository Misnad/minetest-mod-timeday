
local function update_time_days(player, idx)
    local days = minetest.get_day_count()

    local time = minetest.get_timeofday()
    local hours = math.floor(time * 24)
    local minutes = math.floor((time * 24 * 60) % 60)
    local formatted_time = string.format("%02d:%02d", hours, minutes)

    player:hud_change(idx, "text", "Day "..days.." Time "..formatted_time)
    minetest.after(1, update_time_days, player, idx)
end


minetest.register_on_joinplayer(function(player)
    local idx = player:hud_add({
        hud_elem_type = "text",
        position      = {x = 1, y = 1},
        offset        = {x = -20,   y = -20},
        text          = "Welcome!",
        alignment     = {x = -1, y = 0},
        number        = 0xFFFFFF,
    })
    minetest.after(1, update_time_days, player, idx)
end)
